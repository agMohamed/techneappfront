import { Component, OnInit } from '@angular/core';
import { CrudServiceService } from '../crud-service.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Department } from '../Department';
@Component({
  selector: 'app-department-create',
  templateUrl: './department-create.component.html',
  styleUrls: ['./department-create.component.css']
})
export class DepartmentCreateComponent implements OnInit {

  departmentForm : FormGroup;
  routerlink: any;
  constructor(public crudService:CrudServiceService,private fb: FormBuilder,router:Router) {
    this.departmentForm = this.fb.group({
      'deptCode':['', Validators.required],
      'deptName':['', Validators.required],
    })

  }

  ngOnInit(): void {
    this.departmentForm = new FormGroup({
      'deptCode':new FormControl('', [Validators.required]),
      'deptName':new FormControl('', [Validators.required]),
    })

  }
  create(){
    this.crudService.createDepartment(this.departmentForm.value).subscribe(data=>{
      this.routerlink.navigate(["department-list"])
    })
  }

}
