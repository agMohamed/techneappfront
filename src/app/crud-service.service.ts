import { Employee } from './IEmployee';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import{map} from "rxjs/operators";
import{Department} from './Department';


@Injectable({
  providedIn: 'root'
})
export class CrudServiceService {


  protected url='http://localhost:4200';
 // protected url='http://localhost:5001/api/';
  protected url1='http://localhost:5001/api/Department';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private http: HttpClient) { }

  GetEmployee():Observable<Employee[]>{
    return this.http.get<Employee[]>(this.url+'employee')
      .pipe(map(res=>res));
  }
  GetDepartment():Observable<Department[]>{
    return this.http.get<Department[]>(this.url + '/department/')
    .pipe(
      catchError(this.errorHandler))
  }
  createEmployee(data):Observable<Employee>{
    return this.http.post<Employee>(this.url+'/employee/',JSON.stringify(data),{
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8 '),
    })
    .pipe(map(res=>res));

  }
  postEmployee(data):Observable<Employee>{
    return this.http.post<Employee>(this.url + '/employee/', JSON.stringify(data), this.httpOptions)
    .pipe(
      catchError(this.errorHandler))
  }

  postDepartment(data):Observable<Department>{
    return this.http.post<Department>(this.url + '/department/', JSON.stringify(data), this.httpOptions)
    .pipe(
      catchError(this.errorHandler))

  }

  createDepartment(data):Observable<Department>{
    return this.http.post<Department>(this.url,JSON.stringify(data),{
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8 '),
    })
    .pipe(map(res=>res));
  }
  editDepartment(data):Observable<Department>{
    return this.http.put<Department>(this.url+'/department',JSON.stringify(data),{
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8 '),
    })
  }
  deleteDepartment(id):Observable<Department>{
    return this.http.delete<Department>(this.url + '/'+ id)
  }
  deleteEmployee(id):Observable<Employee>{
    return this.http.delete<Employee>(this.url1 + id)
  }
  update(id, data): Observable<Employee> {
    return this.http.put<Employee>(this.url + '/employee/' + id, JSON.stringify(data), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  updateDepartment(code, data): Observable<Department> {
    return this.http.put<Department>(this.url + '/department/' + code, JSON.stringify(data), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  find(id): Observable<Employee> {
    return this.http.get<Employee>(this.url + '/employee/' + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  findd(id): Observable<Department> {
    return this.http.get<Department>(this.url + '/department/' + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
