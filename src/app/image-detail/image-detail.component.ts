import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { CrudServiceService } from '../crud-service.service';
import { Employee } from './../IEmployee';
@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.css']
})
export class ImageDetailComponent implements OnInit {
  id: number;
  employee:Employee;
  constructor(private route: ActivatedRoute,
    private router: Router,public crudService:CrudServiceService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['employeeId'];
    this.crudService.find(this.id).subscribe((data:Employee)=>{
      this.employee=data;
    })
  }

}
