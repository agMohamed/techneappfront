import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudServiceService } from '../crud-service.service';
import { Department } from '../Department';

@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.css']
})
export class DepartmentDetailComponent implements OnInit {
  code: string;
  department:Department;

  constructor(private route: ActivatedRoute,
    private router: Router,public crudService:CrudServiceService) { }

  ngOnInit(): void {
    this.code = this.route.snapshot.params['deptCode'];
    this.crudService.findd(this.code).subscribe((data: Department)=>{
      this.department = data;
    });
  }

}
