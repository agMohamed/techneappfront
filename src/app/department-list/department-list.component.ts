import { Component, OnInit } from '@angular/core';
import { ParamMap, Router } from '@angular/router';
import { CrudServiceService } from '../crud-service.service';
import{Department} from '../Department';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  departments:Department[];
  id:number;
  code:number;
  constructor(public crudService:CrudServiceService,private router:Router,private route: ActivatedRoute) {


  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id')});

    this.crudService.GetDepartment().subscribe((data:Department[])=>{
      this.departments=data;
    })
  }

  deleteDepartment(code){
    this.crudService.deleteDepartment(this.code).subscribe((data)=>{
      this.departments = this.departments.filter(item => item.deptCode !== code)

      this.router.navigateByUrl('/departmentList')
    });
  }
}
