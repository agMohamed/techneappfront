import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from'@angular/forms';

import { ImagesListComponent } from './images-list/images-list.component';
import { ImagesEditComponent } from './images-edit/images-edit.component';
import { ImagesCreateComponent } from './images-create/images-create.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component';
import { DepartmentCreateComponent } from './department-create/department-create.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentDetailComponent } from './department-detail/department-detail.component';
import { ImageDetailComponent } from './image-detail/image-detail.component';
import{Employee} from './IEmployee';
import { Department } from './Department';

@NgModule({
  declarations: [
    AppComponent,
    ImagesListComponent,
    ImagesEditComponent,
    ImagesCreateComponent,
    DepartmentEditComponent,
    DepartmentCreateComponent,
    DepartmentListComponent,
    DepartmentDetailComponent,
    ImageDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    Employee,
    FormsModule,
    Department
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
