import { ImagesListComponent } from './images-list/images-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImagesCreateComponent } from './images-create/images-create.component';
import { ImagesEditComponent } from './images-edit/images-edit.component';
import { DepartmentCreateComponent } from './department-create/department-create.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component';
import { DepartmentDetailComponent } from './department-detail/department-detail.component';

const routes: Routes = [
  {path:'',component:ImagesListComponent},
  {path:'imageList',component:ImagesListComponent},
  {path:'imageCreate',component:ImagesCreateComponent},
  {path:'imageEdit',component:ImagesEditComponent},
  {path:'depatmentCreate',component:DepartmentCreateComponent},
  {path:'departmentList',component:DepartmentListComponent},
  {path:'departmentEdit',component:DepartmentEditComponent},
  {path:'departmentDetail/:id',component:DepartmentDetailComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),

  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
