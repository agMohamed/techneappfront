import { Employee } from './../IEmployee';
import { CrudServiceService } from './../crud-service.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit {
  EmployeeList:any= [];
  employee:Employee[]=[];
  selectedEmployee:Employee;
  id:number;
  constructor(public crudService:CrudServiceService,public router: Router) { }

  ngOnInit() {
    this.loadEmployee();
  }
  loadEmployee(){
    this.crudService.GetEmployee().subscribe((data:Employee[])=>{
      this.EmployeeList=data;
    })
  }
  delete(id){
    this.crudService.deleteEmployee(id).subscribe((data)=>{
      this.employee=this.employee.filter(item=>item.employeeId!==id)

    })
  }

}
