import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from './../IEmployee';
import { CrudServiceService } from './../crud-service.service';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-images-edit',
  templateUrl: './images-edit.component.html',
  styleUrls: ['./images-edit.component.css']
})
export class ImagesEditComponent implements OnInit {
  employeeEditForm:FormGroup;
  id:number;
  employee:Employee;


  constructor(public crudService:CrudServiceService,public router:Router,public route:ActivatedRoute,private fb: FormBuilder) {

      this.employeeEditForm=this.fb.group({
        employeeId:[''],
        fullName:[''],
        nic:[''],
        profilePhoto:[''],
        currentDeptCode:['']
      })
   }

  ngOnInit() {
    this.id = this.route.snapshot.params['EmployeeId'];
    this.crudService.find(this.id).subscribe((data:Employee)=>{
      this.employee=data;
    })

    this.employeeEditForm=new FormGroup({
      employeeId: new FormControl('', [Validators.required]),
      fullName: new FormControl('', [Validators.required]),
      nic: new FormControl('', [Validators.required]),
      profilePhoto: new FormControl('', [Validators.required]),
      currentDeptCode: new FormControl('', [Validators.required])
    })

  }
  submit(){
    this.crudService.update(this.id,this.employeeEditForm).subscribe(res=>{
      this.router.navigateByUrl('imageList');
    })
  }
}
