import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudServiceService } from '../crud-service.service';
import { Department } from '../Department';
@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {

  departmentEditForm:FormGroup;
  deptcode:string;
  department:Department;

  constructor( private fb: FormBuilder, private router : Router,public crudService:CrudServiceService,private route: ActivatedRoute,) {

  }

  ngOnInit(): void {
    this.deptcode= this.route.snapshot.params['deptCode'];
    this.departmentEditForm = new FormGroup({
      'deptCode': new FormControl(['']),
      'deptName':new FormControl([''])
    })

  }

  get f(){
    return this.departmentEditForm.controls;
  }
  submit(){
    console.log(this.departmentEditForm.value);
    this.crudService.updateDepartment(this.deptcode, this.departmentEditForm.value).subscribe(res => {

         this.router.navigateByUrl('/department-list');
    })
  }


}
