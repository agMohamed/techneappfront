import { Employee } from './../IEmployee';
import { Component, OnInit, ViewChild } from '@angular/core';
import{FormGroup,FormControl, Validators,FormsModule, NgForm,FormBuilder} from '@angular/forms';
import { CrudServiceService } from '../crud-service.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-images-create',
  templateUrl: './images-create.component.html',
  styleUrls: ['./images-create.component.css']
})
export class ImagesCreateComponent implements OnInit {
  imgSrc:string='/assets/img/a1.png';
  selectedImage:any=null;
  isSubmitted:boolean=false;
  employee:Employee;

  @ViewChild('form') uf:NgForm;

  employeeForm : FormGroup;


  constructor(public crudService:CrudServiceService,private router :Router,private fb: FormBuilder) {

    this.employeeForm=this.fb.group({
    employeeId:new FormControl('',Validators.required),
    fullName:new FormControl('',Validators.required),
    nic:new FormControl('',Validators.required),
    imagename:new FormControl(''),
    Password:new FormControl('',Validators.required),
    profilePhoto:new FormControl(''),
    CurrentDeptCode:new FormControl('',Validators.required)

    })
  }

  ngOnInit() {
    this.onSubmit();
  }
  showPreview(event:any){
    if(event.target.files && event.target.files[0]){
      const reader =new FileReader();
      reader.onload=(e:any)=> this.imgSrc=e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage=event.target.files[0];
    }
    else{
      this.imgSrc='/assets/img/a1.png';
      this.selectedImage=null;
    }
  }

  onSubmit(){
    this.isSubmitted=true;

    this.crudService.createEmployee(this.employeeForm.value).subscribe((data=>{
      this.employee=data;
      this.router.navigateByUrl('image-list');
   }))
  }
}
